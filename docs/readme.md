# SOLAR API Server

## REST API

### Domain *

| Operation |  Method | URL                              |
|-----------|---------|----------------------------------|
| create    | POST    | /domain                          |
| list      | GET     | /domain                          |
| read      | GET     | /domain/[id]                     |
| update    | PUT     | /domain/[id]                     |
| delete    | DELETE  | /domain/[id]                     |

### Domain Component *

| Operation |  Method | URL                                   |
|-----------|---------|---------------------------------------|
| create    | POST    | /domain/[id]/component                |
| list      | GET     | /domain/[id]/component                |
| read      | GET     | /domain/[id]/component/[id]           |
| update    | PUT     | /domain/[id]/component/[id]           |
| delete    | DELETE  | /domain/[id]/component/[id]           |

### Domain Component Relation

| Operation |  Method | URL                                      |
|-----------|---------|------------------------------------------|
| create    | POST    | /domain/[id]/component[id]/relation      |
| list      | GET     | /domain/[id]/component[id]/relation      |
| read      | GET     | /domain/[id]/component[id]/relation/[id] |
| update    | PUT     | /domain/[id]/component[id]/relation/[id] |
| delete    | DELETE  | /domain/[id]/component[id]/relation/[id] |

### Domain Architecture *

| Operation |  Method | URL                                   |
|-----------|---------|---------------------------------------|
| create    | POST    | /domain/[id]/architecture             |
| list      | GET     | /domain/[id]/architecture             |
| read      | GET     | /domain/[id]/architecture/[id]        |
| update    | PUT     | /domain/[id]/architecture/[id]        |
| delete    | DELETE  | /domain/[id]/architecture/[id]        |

### Domain Architecture Element

| Operation |  Method | URL                                         |
|-----------|---------|---------------------------------------------|
| create    | POST    | /domain/[id]/architecture/[id]/element      |
| list      | GET     | /domain/[id]/architecture/[id]/element      |
| read      | GET     | /domain/[id]/architecture/[id]/element/[id] |
| update    | PUT     | /domain/[id]/architecture/[id]/element/[id] |
| delete    | DELETE  | /domain/[id]/architecture/[id]/element/[id] |

### Domain Architecture Element Version *

| Operation |  Method | URL                                                      |
|-----------|---------|----------------------------------------------------------|
| create    | POST    | /domain/[id]/architecture/[id]/element/[id]/cluster      |
| list      | GET     | /domain/[id]/architecture/[id]/element/[id]/cluster      |
| read      | GET     | /domain/[id]/architecture/[id]/element/[id]/cluster/[id] |
| update    | PUT     | /domain/[id]/architecture/[id]/element/[id]/cluster/[id] |
| delete    | DELETE  | /domain/[id]/architecture/[id]/element/[id]/cluster/[id] |

### Domain Architecture Element Version Relation

| Operation |  Method | URL                                                                    |
|-----------|---------|------------------------------------------------------------------------|
| create    | POST    | /domain/[id]/architecture/[id]/element/[id]/cluster/[id]/relation      |
| list      | GET     | /domain/[id]/architecture/[id]/element/[id]/cluster/[id]/relation      |
| read      | GET     | /domain/[id]/architecture/[id]/element/[id]/cluster/[id]/relation/[id] |
| update    | PUT     | /domain/[id]/architecture/[id]/element/[id]/cluster/[id]/relation/[id] |
| delete    | DELETE  | /domain/[id]/architecture/[id]/element/[id]/cluster/[id]/relation/[id] |

### Domain Solution *

| Operation |  Method | URL                                   |
|-----------|---------|---------------------------------------|
| create    | POST    | /domain/[id]/solution                 |
| list      | GET     | /domain/[id]/solution                 |
| read      | GET     | /domain/[id]/solution/[id]            |
| update    | PUT     | /domain/[id]/solution/[id]            |
| delete    | DELETE  | /domain/[id]/solution/[id]            |

### Domain Solution Element

| Operation |  Method | URL                                         |
|-----------|---------|---------------------------------------------|
| create    | POST    | /domain/[id]/solution/[id]/element          |
| list      | GET     | /domain/[id]/solution/[id]/element          |
| read      | GET     | /domain/[id]/solution/[id]/element/[id]     |
| update    | PUT     | /domain/[id]/solution/[id]/element/[id]     |
| delete    | DELETE  | /domain/[id]/solution/[id]/element/[id]     |

### Domain Solution Element Version *

| Operation |  Method | URL                                                      |
|-----------|---------|----------------------------------------------------------|
| create    | POST    | /domain/[id]/solution/[id]/element/[id]/cluster          |
| list      | GET     | /domain/[id]/solution/[id]/element/[id]/cluster          |
| read      | GET     | /domain/[id]/solution/[id]/element/[id]/cluster/[id]     |
| update    | PUT     | /domain/[id]/solution/[id]/element/[id]/cluster/[id]     |
| delete    | DELETE  | /domain/[id]/solution/[id]/element/[id]/cluster/[id]     |

### Domain Solution Element Version Relation

| Operation |  Method | URL                                                                    |
|-----------|---------|------------------------------------------------------------------------|
| create    | POST    | /domain/[id]/solution/[id]/element/[id]/cluster/[id]/relation          |
| list      | GET     | /domain/[id]/solution/[id]/element/[id]/cluster/[id]/relation          |
| read      | GET     | /domain/[id]/solution/[id]/element/[id]/cluster/[id]/relation/[id]     |
| update    | PUT     | /domain/[id]/solution/[id]/element/[id]/cluster/[id]/relation/[id]     |
| delete    | DELETE  | /domain/[id]/solution/[id]/element/[id]/cluster/[id]/relation/[id]     |

### Domain Solution Element Version Entity *

| Operation |  Method | URL                                                                  |
|-----------|---------|----------------------------------------------------------------------|
| create    | POST    | /domain/[id]/solution/[id]/element/[id]/cluster/[id]/entity          |
| list      | GET     | /domain/[id]/solution/[id]/element/[id]/cluster/[id]/entity          |
| read      | GET     | /domain/[id]/solution/[id]/element/[id]/cluster/[id]/entity/[id]     |
| update    | PUT     | /domain/[id]/solution/[id]/element/[id]/cluster/[id]/entity/[id]     |
| delete    | DELETE  | /domain/[id]/solution/[id]/element/[id]/cluster/[id]/entity/[id]     |


### Domain Solution Element Version Entity Relation

| Operation |  Method | URL                                                                           |
|-----------|---------|-------------------------------------------------------------------------------|
| create    | POST    | /domain/[id]/solution/[id]/element/[id]/cluster/[id]/entity/relation          |
| list      | GET     | /domain/[id]/solution/[id]/element/[id]/cluster/[id]/entity/relation          |
| read      | GET     | /domain/[id]/solution/[id]/element/[id]/cluster/[id]/entity/relation/[id]     |
| update    | PUT     | /domain/[id]/solution/[id]/element/[id]/cluster/[id]/entity/relation/[id]     |
| delete    | DELETE  | /domain/[id]/solution/[id]/element/[id]/cluster/[id]/entity/relation/[id]     |

### Domain Task

| Operation |  Method | URL                                   |
|-----------|---------|---------------------------------------|
| create    | POST    | /domain/[id]/task                     |
| list      | GET     | /domain/[id]/task                     |
| read      | GET     | /domain/[id]/task/[id]                |
| update    | PUT     | /domain/[id]/task/[id]                |
| delete    | DELETE  | /domain/[id]/task/[id]                |
