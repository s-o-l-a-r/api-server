//server.js

// establish connection to database
const DATABASE = require("./app/database")
DATABASE.connect()

// start API server
const APISERVER = require("./app");
const PORT      = process.env.PORT || APISERVER.config.port

APISERVER.app.listen(PORT, () => {
  console.log(`SOLAR API Server is running on port ${PORT}.`)
});
