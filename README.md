# API Server

SOLAR API Server

## Development

## Testing

The tests make use of JEST and supertest and are implemented following the guidelines as described here:

https://www.albertgao.xyz/2017/05/24/how-to-test-expressjs-with-jest-and-supertest/

To execute the tests ensure that a MongoDB is available on localhost and run:

````
npm run test
````
