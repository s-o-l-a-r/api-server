const express        = require("express")
const bodyParser     = require("body-parser")
const cors           = require("cors")
const webConfig      = require("./app/config/web.config")
const winston        = require("winston")
const expressWinston = require('express-winston')

const domainRouter   = require("./app/routes/domain.routes")
const blockRouter    = require("./app/routes/block.routes")

//------------------------------------------------------------------------------
// CODE
//------------------------------------------------------------------------------

// create app
var app = new express();

// set standard logging
// app.use( expressWinston.logger({
//   transports: [
//     new winston.transports.Console({
//       json:     true,
//       colorize: true
//     })
//   ]
// }));

// set cors options
app.use( cors(webConfig.cors) );

// parse requests of content-type - application/json
app.use( bodyParser.json() );

// parse requests of content-type - application/x-www-form-urlencoded
app.use( bodyParser.urlencoded({ extended: true }) );

// add routes
app.use('/domain', domainRouter);
app.use('/block',  blockRouter);

// set error logging
// app.use(expressWinston.logger({
//   transports: [
//     new winston.transports.Console({
//       json: true,
//       colorize: true
//     })
//   ]
// }));

module.exports = {
  app:    app,
  config: webConfig
}
