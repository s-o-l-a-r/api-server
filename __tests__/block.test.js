const database  = require("../app/database");
const request   = require("supertest");
const APISERVER = require("../app");

// -----------------------------------------------------------------------------
//
// Block Schema:
//   _id:                   uuid
//   domain:                uuid of domain
//   name:                  string
//   description:           string
//   configuration:         string (schema / template / defaults)
//   versions:              array
//     - version:           semver
//       description:       string
//       configuration:     string (schema / template / defaults)
//       relations:         array
//         - name:          string
//           type:          string
//           multiplicity:  string
//           configuration: string (schema / template / defaults)
//           block:         uuid
//           version:       semver
//
// -----------------------------------------------------------------------------

var testdata_domain  = { "_id": "274ecf1f-6d49-4768-bfba-fb43ab9f7055", "name": "Test","description": "Test domain"}
var testdata_block_1 = {
  _id:           "815f0da3-bc79-4c25-9b8d-3cde7d030313",
  domain:        "274ecf1f-6d49-4768-bfba-fb43ab9f7055",
  name:          "Block A",
  description:   "First test block",
  configuration: "{{name}}",
  versions: [{
    version:       "1.0.0",
    description:   "First version",
    configuration: "{{version}}",
    relations: [{
      name:          "parent",
      type:          "context",
      multiplicity:  "0-1",
      configuration: "{{uuid}}",
      block:         "815f0da3-bc79-4c25-9b8d-3cde7d030313",
      version:       ">1.0.0"
    }]
  }]
}
var testdata_block_2 = {
  _id:           "815f0da3-bc79-4c25-9b8d-3cde7d030313",
  domain:        "274ecf1f-6d49-4768-bfba-fb43ab9f7055",
  name:          "Block A",
  description:   "First test block",
  configuration: "{{name}}",
  versions: [{
    version:       "1.0.0",
    description:   "First version",
    configuration: "{{version}}",
    relations: [{
      name:          "parent",
      type:          "context",
      multiplicity:  "0-1",
      configuration: "{{uuid}}",
      block:         "815f0da3-bc79-4c25-9b8d-3cde7d030313",
      version:       ">1.0.0"
    }]
  },{
    version:       "1.0.1",
    description:   "First version (patch)",
    configuration: "{{version}}",
    relations: [{
      name:          "parent",
      type:          "context",
      multiplicity:  "0-1",
      configuration: "{{uuid}}",
      block:         "815f0da3-bc79-4c25-9b8d-3cde7d030313",
      version:       ">1.0.0"
    },{
      name:          "service",
      type:          "service",
      multiplicity:  "0-N",
      configuration: "{{uuid}}",
      block:         "815f0da3-bc79-4c25-9b8d-3cde7d030313",
      version:       ">1.0.0"
    }]
  }]
}

describe("Test block related API calls", done => {
  // setup
  beforeAll(() => {
    return database.connect()
    // .then(
    // request(APISERVER.app)
    //   .post("/domain")
    //   .send(testdata_domain)
    //   .set('Accept', 'application/json')
    // )}
  });

  // cleanup
  afterAll(() => {
    return database.disconnect()

    // return request(APISERVER.app)
    //   .delete("/domain/" + testdata_block_2._id)
    //   .then(response => {
    //     database.disconnect(done);
    //   });
  });

  // test 1: list all blocks
  test("List Blocks", done => {
    request(APISERVER.app)
      .get("/block/" + testdata_block_1.domain)
      .then(response => {
        expect(response.statusCode).toBe(200);
        done();
      });
  });

  // test 2: create a block
  test("Create a block", done => {
    request(APISERVER.app)
      .post("/block/" + testdata_block_1.domain)
      .send(testdata_block_1)
      .set('Accept', 'application/json')
      .then(response => {
        expect(response.statusCode).toBe(200);
        done();
      });
  });

  // test 3: update a block
  test("Update a block", done => {
    request(APISERVER.app)
      .put("/block/" + testdata_block_2.domain + "/" + testdata_block_2._id)
      .send(testdata_block_2)
      .set('Accept', 'application/json')
      .then(response => {
        if (response.statusCode != 200) {
          console.log(JSON.stringify(testdata_block_2))
          console.log(response.error)
        }
        expect(response.statusCode).toBe(200);
        done();
      });
  });

  // test 4: read a block
  test("Read a block", done => {
    request(APISERVER.app)
      .get("/block/" + testdata_block_2.domain + "/" + testdata_block_2._id)
      .then(response => {
        expect(response.statusCode).toBe(200);
        done();
      });
  });

  // test 5: delete a block
  test("Delete a block", done => {
    request(APISERVER.app)
      .delete("/block/" + testdata_block_2.domain + "/" + testdata_block_2._id)
      .then(response => {
        expect(response.statusCode).toBe(200);
        done();
      });
  });
});
