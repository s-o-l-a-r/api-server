const database  = require("../app/database");
const request   = require("supertest");
const APISERVER = require("../app");

var testdata_domain_1 = {"_id": "274ecf1f-6d49-4768-bfba-fb43ab9f7055", "name": "Test","description": "A test domain"}
var testdata_domain_2 = {"_id": "274ecf1f-6d49-4768-bfba-fb43ab9f7055", "name": "Test - updated","description": "A test domain - updated"}

describe("Test domain related API calls", () => {
  // setup
  beforeAll(() => {
    return database.connect()
  });

  // cleanup
  afterAll(() => {
    return database.disconnect()
  });

  // test 1: list all domains
  test("List Domains", done => {
    request(APISERVER.app)
      .get("/domain")
      .then(response => {
        expect(response.statusCode).toBe(200);
        done();
      });
  });

  // test 2: create a domain
  test("Create a domain", done => {
    request(APISERVER.app)
      .post("/domain")
      .send(testdata_domain_1)
      .set('Accept', 'application/json')
      .then(response => {
        expect(response.statusCode).toBe(200);
        done();
      });
  });

  // test 3: update a domain
  test("Update a domain", done => {
    request(APISERVER.app)
      .put("/domain/" + testdata_domain_2._id)
      .send(testdata_domain_2)
      .set('Accept', 'application/json')
      .then(response => {
        expect(response.statusCode).toBe(200);
        done();
      });
  });

  // test 4: read a domain
  test("Read a domain", done => {
    request(APISERVER.app)
      .get("/domain/" + testdata_domain_2._id)
      .then(response => {
        expect(response.statusCode).toBe(200);
        done();
      });
  });

  // test 5: delete a domain
  test("Delete a domain", done => {
    request(APISERVER.app)
      .delete("/domain/" + testdata_domain_2._id)
      .then(response => {
        expect(response.statusCode).toBe(200);
        done();
      });
  });

});
