const domain  = require("../controllers/domain.controller.js")
const express = require("express")

// ----- SETUP -----

// Create router
var router = express.Router();

// Create a new domain
router.post("/", domain.create);

// List domains
router.get("/", domain.list);

// Read a single domain with id
router.get("/:domain", domain.read);

// Update a domain with id
router.put("/:domain", domain.update);

// Delete a domain with id
router.delete("/:domain", domain.delete);

// ----- EXPORTS -----

module.exports = router;
