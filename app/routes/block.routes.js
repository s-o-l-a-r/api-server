const block  = require("../controllers/block.controller.js")
const express = require("express")

// ----- SETUP -----

// Create router
var router = express.Router();

// Create a new block
router.post("/:domain", block.create);

// List blocks
router.get("/:domain", block.list);

// Read a single block with id
router.get("/:domain/:block", block.read);

// Update a block with id
router.put("/:domain/:block", block.update);

// Delete a block with id
router.delete("/:domain/:block", block.delete);

// ----- EXPORTS -----

module.exports = router;
