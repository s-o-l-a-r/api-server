const uuid     = require("uuid")
const database = require("../database")
const Ajv      = require("ajv")

const ajv      = new Ajv()

// -----------------------------------------------------------------------------
//
// Domain Schema:
//   _id:         uuid
//   name:        string
//   description: string
//
// -----------------------------------------------------------------------------

const COLLECTION = "domain"

const domain_schema = {
  type: "object",
  properties: {
    _id:         {type: "string"},
    name:        {type: "string"},
    description: {type: "string"}
  },
  required:             ["name"],
  additionalProperties: false,
}

// -----------------------------------------------------------------------------
// Process request and retrieve result / error messages
// -----------------------------------------------------------------------------
function process_request(req, schema, parameters) {
  var result = {
    valid: true,
    code:  200,
    msg:   "OK",
    pos:   "",
    obj:   null
  }

  // check if all required parameters have been made available
  if (parameters) {
    var missing = []

    for (var parameter of parameters) {
      if (!(parameter in req.params)) {
        missing.push(parameter)
      }
    }

    // check if parameters were missing
    if (missing.length > 0) {
      result.valid = false
      result.code  =  400
      result.msg   =  "Missing parameters: " + missing.toString()
      return result
    }
  }

  // check if body has been defined
  if ( !req.body ) {
    result.valid = false
    result.code  =  400
    result.msg   =  "body is undefined"
    return result
  }

  // validate body schema if needed
  if (schema) {
    // check if body complies with Schema
    var validate = ajv.compile(schema)

    // check if validation errors have occured
    if ( !validate(req.body) ) {
      result.valid = false
      result.code  =  400
      result.msg   = ""

      for (const err of validate.errors) {
        result.msg += err.message + "\n"
      }

      return result
    }

    // convert body to object
    result.obj = req.body
  }

  // validation completed
  return result
}

// -----------------------------------------------------------------------------
// Create a new domain
// -----------------------------------------------------------------------------
exports.create = (req, res) => {
  var req2 = process_request(req, domain_schema)

  // check for errors
  if ( !req2.valid ) {
    res.status(req2.code).send(req2.msg)

    return
  }

  // prepare domain object
  var domain = req2.obj

  domain._id         = domain._id         || uuid.v4()
  domain.description = domain.description || ""

  // save domain in the database
  var collection = database.solar.collection(COLLECTION);

  collection.insertOne(domain)
    .then(data => {
      res.status(req2.code).send(domain)
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the domain."
      });
    });
};

// -----------------------------------------------------------------------------
// List domains
// -----------------------------------------------------------------------------
exports.list = (req, res) => {
  var collection = database.solar.collection(COLLECTION);

  const cursor = collection.find();

  cursor.toArray()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while listing the domains."
      });
    });
};

// -----------------------------------------------------------------------------
// Read a single domain with id
// -----------------------------------------------------------------------------
exports.read = (req, res) => {
  var req2 = process_request(req, null, ['domain'])

  // check for errors
  if ( !req2.valid ) {
    res.status(req2.code).send(req2.msg)

    return
  }

  // determine parameter
  const _id = req.params.domain;

  var collection = database.solar.collection(COLLECTION);

  collection.findOne({_id: _id})
     .then(data => {
       if (!data)
         res.status(404).send({ message: `Domain with _id=${_id} has not been found!` });
       else
         res.send(data);
     })
     .catch(err => {
       res
         .status(500)
         .send({ message: `Error retrieving domain with _id=${_id}` });
     });
};

// -----------------------------------------------------------------------------
// Update a domain with id
// -----------------------------------------------------------------------------
exports.update = (req, res) => {
  var req2 = process_request(req, domain_schema, ['domain'])

  // check body for errors
  if ( !req2.valid ) {
    res.status(req2.code).send(req2.msg)

    return
  }

  // determine domain _id
  const _id = req.params.domain;

  // prepare domain object
  var domain = req2.obj

  domain._id         = domain._id         || _id
  domain.description = domain.description || ""

  // update domain in the database
  var collection = database.solar.collection(COLLECTION);

  collection.updateOne({_id: _id}, {$set: domain}, { upsert: true })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update domain with _id=${_id}. Maybe domain was not found!`
        });
      } else
        res.send(domain);
    })
    .catch(err => {
      console.log(err)
      res.status(500).send({
        message: `Error updating domain with _id=${_id}`
      });
    });
};

// -----------------------------------------------------------------------------
// Delete a domain with id
// -----------------------------------------------------------------------------
exports.delete = (req, res) => {
  var req2 = process_request(req, null, ['domain'])

  // check for errors
  if ( !req2.valid ) {
    res.status(req2.code).send(req2.msg)

    return
  }

  // determine parameter
  const _id = req.params.domain;

  // delete domain
  var collection = database.solar.collection(COLLECTION);

  collection.deleteOne({_id: _id})
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete domain with _id=${_id}. Maybe domain was not found!`
        });
      }
      else {
        res.send({
          message: "Domain was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: `Could not delete domain with _id=${_id}!`
      });
    });
};
