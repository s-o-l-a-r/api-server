const uuid     = require("uuid")
const database = require("../database")
const Ajv      = require("ajv")
const semver   = require("semver")

const ajv      = new Ajv()

ajv.addFormat("semver", (data) => semver.validRange(data) != null)

// -----------------------------------------------------------------------------
//
// Block Schema:
//   _id:                   uuid
//   domain:                uuid of domain
//   name:                  string
//   description:           string
//   configuration:         string (schema / template / defaults)
//   versions:              array
//     - version:           semver
//       description:       string
//       configuration:     string (schema / template / defaults)
//       relations:         array
//         - name:          string
//           type:          string
//           multiplicity:  string
//           configuration: string (schema / template / defaults)
//           block:         uuid
//           version:       semver
//
// -----------------------------------------------------------------------------

const COLLECTION = "block"


const relation_schema = {
  type: "object",
  properties: {
    name:          {type: "string"},
    type:          {type: "string", enum: ["context", "service", "management"]},
    multiplicity:  {type: "string", enum: ["0-1", "1", "0-N", "1-N"]},
    configuration: {type: "string"},
    block:         {type: "string"},
    version:       {type: "string", format: "semver"}
  },
  required: ["name", "type", "multiplicity", "block", "version"]
}

const version_schema = {
  type: "object",
  properties: {
    version:       {type: "string", format: "semver"},
    description:   {type: "string"},
    configuration: {type: "string"},
    relations:     {
      type:     "array",
      items:    relation_schema
    }
  },
  required: ["version"]
}

const block_schema = {
  type: "object",
  properties: {
    _id:           {type: "string"},
    domain:        {type: "string"},
    name:          {type: "string"},
    description:   {type: "string"},
    configuration: {type: "string"},
    versions:      {
      type:     "array",
      items:    version_schema
    },
  },
  required:             ["name"],
  additionalProperties: false,
}

// -----------------------------------------------------------------------------
// Process request and retrieve result / error messages
// -----------------------------------------------------------------------------
function process_request(req, schema, parameters) {
  var result = {
    valid: true,
    code:  200,
    msg:   "OK",
    pos:   "",
    obj:   null
  }

  // check if all required parameters have been made available
  if (parameters) {
    var missing = []

    for (var parameter of parameters) {
      if (!(parameter in req.params)) {
        missing.push(parameter)
      }
    }

    // check if parameters were missing
    if (missing.length > 0) {
      result.valid = false
      result.code  =  400
      result.msg   =  "Missing parameters: " + missing.toString()
      return result
    }
  }

  // check if body has been defined
  if ( !req.body ) {
    result.valid = false
    result.code  =  400
    result.msg   =  "body is undefined"
    return result
  }

  // validate body schema if needed
  if (schema) {
    // check if body complies with Schema
    var validate = ajv.compile(schema)

    // check if validation errors have occured
    if ( !validate(req.body) ) {
      result.valid = false
      result.code  =  400
      result.msg   = ""

      for (const err of validate.errors) {
        result.msg += "Location: " + err.instancePath + "\nData:" + JSON.stringify(err.params) + "\nError: " + err.message + "\n"
      }

      return result
    }

    // convert body to object
    result.obj = req.body
  }

  // validation completed
  return result
}

// -----------------------------------------------------------------------------
// Create a new block
// -----------------------------------------------------------------------------
exports.create = (req, res) => {
  var req2 = process_request(req, block_schema, ['domain'])

  // check for errors
  if ( !req2.valid ) {
    res.status(req2.code).send(req2.msg)

    return
  }

  // prepare block object
  var block = req2.obj

  block._id           = block._id           || uuid.v4()
  block.domain        = req.params.domain
  block.description   = block.description   || ""
  block.configuration = block.configuration || ""
  block.versions      = block.versions      || []

  for (var version of block.versions) {
    version.description   = version.description   || ""
    version.configuration = version.configuration || ""
    version.relations     = version.relations     || []
    for (var relation of version.relations) {
      relation.configuration = relation.configuration || ""
    }
  }

  // save block in the database
  var collection = database.solar.collection(COLLECTION);

  collection.insertOne(block)
    .then(data => {
      res.status(req2.code).send(block)
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the block."
      });
    });
};

// -----------------------------------------------------------------------------
// List blocks
// -----------------------------------------------------------------------------
exports.list = (req, res) => {
  var req2 = process_request(req, null, ['domain'])

  // check for errors
  if ( !req2.valid ) {
    res.status(req2.code).send(req2.msg)

    return
  }

  // find from block belonging to a domain
  var collection = database.solar.collection(COLLECTION);

  const cursor = collection.find({domain: req.params.domain});

  cursor.toArray()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      console.log(err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while listing the blocks."
      });
    });

};

// -----------------------------------------------------------------------------
// Read a single block with id
// -----------------------------------------------------------------------------
exports.read = (req, res) => {
  var req2 = process_request(req, null, ['domain', 'block'])

  // check for errors
  if ( !req2.valid ) {
    res.status(req2.code).send(req2.msg)

    return
  }

  // determine parameters
  const domain = req.params.domain;
  const _id    = req.params.block;

  // read block
  var collection = database.solar.collection(COLLECTION);

  collection.findOne({_id: _id, domain: domain})
     .then(data => {
       if (!data)
         res.status(404).send({ message: `Block with _id=${_id} within the domain=${domain} has not been found!` });
       else
         res.send(data);
     })
     .catch(err => {
       res
         .status(500)
         .send({ message: `Error retrieving block with _id=${_id} from domain=${domain}` });
     });
};

// -----------------------------------------------------------------------------
// Update a block with id
// -----------------------------------------------------------------------------
exports.update = (req, res) => {
  var req2 = process_request(req, block_schema, ['domain', 'block'])

  // check body for errors
  if ( !req2.valid ) {
    res.status(req2.code).send(req2.msg)

    return
  }

  // determine parameters
  const domain = req.params.domain;
  const _id    = req.params.block;

  // prepare block object
  var block = req2.obj

  block._id           = block._id           || uuid.v4()
  block.domain        = req.params.domain
  block.description   = block.description   || ""
  block.configuration = block.configuration || ""
  block.versions      = block.versions      || []

  for (var version of block.versions) {
    version.description   = version.description   || ""
    version.configuration = version.configuration || ""
    version.relations     = version.relations     || []
    for (var relation of version.relations) {
      relation.configuration = relation.configuration || ""
    }
  }

  // update block in the database
  var collection = database.solar.collection(COLLECTION);

  collection.updateOne({_id: _id, domain: domain}, {$set: block}, { upsert: true })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update block with _id=${_id} within domain=${domain}. Maybe block was not found!`
        });
      } else
        res.send(block);
    })
    .catch(err => {
      console.log(err)
      res.status(500).send({
        message: `Error updating block with _id=${_id} within domain=${domain}`
      });
    });
};

// -----------------------------------------------------------------------------
// Delete a block with id
// -----------------------------------------------------------------------------
exports.delete = (req, res) => {
  var req2 = process_request(req, null, ['domain', 'block'])

  // check for errors
  if ( !req2.valid ) {
    res.status(req2.code).send(req2.msg)

    return
  }

  // determine parameters
  const domain = req.params.domain;
  const _id    = req.params.block;

  // delete block
  var collection = database.solar.collection(COLLECTION);

  collection.deleteOne({_id: _id, domain: domain})
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete block with uuid=${uuid} within domain=${domain}. Maybe block was not found!`
        });
      }
      else {
        res.send({
          message: "Block was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: `Could not delete block with uuid=${uuid} within domain=${domain}!`
      });
    });
};
