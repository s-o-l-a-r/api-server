const mongodb = require("mongodb")
const config  = require("../config/database.config.js")

// ----- SETUP -----

// MongoDB client
const client = new mongodb.MongoClient(config.url, config.options)

// exported database handle
const database = {
  client:     client,      // mongodb client
  solar:      null,        // solar database
  connect:    connect,     // method to connect to database
  disconnect: disconnect,  // method to disconnect from database
  connected:  false        // indicates if a connection has been established
}

// connect: function to establish a connection to the database if needed
async function connect() {
  if (!database.connected) {
    try {
      await database.client.connect()
    } catch (err) {
      database.solar     = null
      database.connected = false

      console.log("Cannot connect to the database: " + err)
      process.exit()
    }

    // create link to solar database
    database.solar = client.db(config.database)

    // update connection flag
    database.connected = true
  }
}

// disconnect: function to close a connection to the database if needed
async function disconnect() {
  if (database.connected) {
    try {
      await database.client.close()
    } catch (err) {
      database.solar     = null
      database.connected = false

      console.log("Cannot close connection to the database: " + err)
      process.exit();
    }

    // create link to solar database
    database.solar = null

    // update connection flag
    database.connected = false
  }
}

// ----- EXPORTS -----

module.exports = database
