module.exports = {
  url: "mongodb://127.0.0.1:27017",
  database: "solar",
  options: {
    useNewUrlParser:    true,
    useUnifiedTopology: true,
    writeConcern: {
      w: 'majority'
    }
  }
};
